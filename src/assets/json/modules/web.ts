export default {
	title: 'H5资源',
	sort: 5,
	nav: [
		{
			text: '无界',
			link: 'https://wujie-micro.github.io/doc/',
			icon: 'https://wujie-micro.github.io/doc/wujie.svg',
			desc: '极致前端微框架'
		},
		{
			text: 'qiankun',
			link: 'https://qiankun.umijs.org/zh',
			icon: 'https://gw.alipayobjects.com/zos/bmw-prod/8a74c1d3-16f3-4719-be63-15e467a68a24/km0cv8vn_w500_h500.png',
			desc: '阿里微前端解决方案'
		},
		{
			text: 'xy-ui',
			link: 'https://xy-ui.codelabo.cn/docs/#/',
			desc: '跨端UI'
		},
		{
			text: 'Driver.js',
			link: 'https://kamranahmed.info/driver.js/',
			icon: 'https://kamranahmed.info/driver.js/images/driver.png',
			desc: '交互引导工具'
		},
		{
			text: 'lulu UI',
			link: 'https://l-ui.com/',
			desc: 'webComponent组件库'
		},
		{
			text: 'better-scroll',
			link: 'https://better-scroll.github.io/docs/zh-CN/',
			icon: 'https://dpubstatic.udache.com/static/dpubimg/t_L6vAgQ-E/logo.svg',
			desc: '移动端滚动插件'
		},
		{
			text: 'Cleave.js',
			link: 'https://nosir.github.io/cleave.js/',
			desc: '输入框格式化插件'
		},
		{
			text: 'egjs',
			link: 'https://naver.github.io/egjs/',
			icon: 'https://naver.github.io/egjs/img/logotype1_black.svg',
			desc: '动态元素合集'
		},
		{
			text: 'logic-flow',
			link: 'http://logic-flow.org/',
			icon: 'http://logic-flow.org/new-logo.svg',
			desc: '流程图插件'
		},
		{
			text: 'sheetjs',
			link: 'https://sheetjs.com/',
			icon: 'https://sheetjs.com/favico/favicon-32x32.png'
		},
		{
			text: 'x-spread',
			link: 'https://hondrytravis.com/x-spreadsheet-doc/',
			desc: '快速构建 Web Excel'
		},
		{
			text: 'pnpm',
			link: 'https://pnpm.io/zh/',
			icon: 'https://pnpm.io/zh/img/favicon.png',
			desc: '包管理工具'
		},
		{
			text: 'Node',
			link: 'https://nodejs.org/en/',
			icon: 'https://nodejs.org/static/images/favicons/favicon-32x32.png'
		},
		{
			text: 'prettier',
			link: 'https://prettier.io/',
			icon: 'https://prettier.io/icon.png',
			desc: '代码格式化工具'
		},
		{
			text: '100px',
			link: 'https://100px.net/',
			icon: 'https://100px.net/logo.png',
			desc: 'H5各类抽奖插件'
		},
		{
			text: 'rrweb',
			link: 'https://www.rrweb.io/',
			icon: 'https://www.rrweb.io/favicon.png',
			desc: '前端录屏工具'
		},
		{
			text: 'Animista',
			link: 'https://animista.net/',
			icon: 'https://animista.net/favicon-32x32.png',
			desc: '一个动画工具'
		},
		{
			text: 'loading',
			link: 'https://loading.io/',
			desc: '各种loading示例'
		},
		{
			text: 'TweenMax',
			link: 'https://www.tweenmax.com.cn/',
			desc: '动画插件'
		},
		{
			text: 'skrollr.js',
			link: 'http://prinzhorn.github.io/skrollr/',
			desc: '滚动效果插件'
		},
		{
			text: 'Granim.js',
			link: 'https://sarcadass.github.io/granim.js/index.html',
			desc: '渐变动画插件'
		},
		{
			text: 'hover视差',
			link: 'http://gijsroge.github.io/tilt.js/'
		},
		{
			text: '滑动隐藏头部',
			link: 'http://wicky.nillia.ms/headroom.js/'
		},
		{
			text: 'introjs',
			link: 'https://introjs.com/',
			icon: 'https://introjs.com/img/logo.svg',
			desc: '产品引导库'
		},
		{
			text: '按钮hover',
			link: 'https://varin6.github.io/Hover-Buttons/'
		},
		{
			text: 'Swiper',
			link: 'http://www.swiper.com.cn/',
			icon: 'https://swiperjs.com/images/share-banner-new.png'
		},
		{
			text: 'Fabric',
			link: 'http://fabricjs.com/',
			desc: 'canvas工具'
		},
		{
			text: 'threeJS',
			link: 'https://threejs.org/',
			icon: 'https://threejs.org/files/favicon.ico'
		},
		{
			text: 'barba',
			link: 'https://barba.js.org/',
			icon: 'https://barba.js.org/favicons/apple-touch-icon.png?rev=1',
			desc: '页面平滑过渡'
		},
		{
			text: 'RELLAX.js',
			link: 'https://dixonandmoe.com/rellax/',
			desc: '轻量级滚动视差'
		},
		{
			text: 'Scrollissimo',
			link: 'https://promo.github.io/scrollissimo/',
			desc: '滚动动效设计工具'
		},
		{
			text: 'matter.js',
			link: 'https://brm.io/matter-js/',
			desc: '2D物理动效库',
			icon: 'https://brm.io/img/icon/favicon-32x32.png'
		},
		{
			text: 'favico.js',
			link: 'http://lab.ejci.net/favico.js/',
			desc: '让你的favico动起来'
		},
		{
			text: 'ts.config配置',
			link: 'https://blog.csdn.net/weixin_30758821/article/details/101713657',
			icon: 'https://www.typescriptlang.org/icons/icon-96x96.png?v=8944a05a8b601855de116c8a56d3b3ae'
		},
		{
			text: '阮一峰TS教程',
			link: 'https://ts.xcatliu.com/',
			icon: 'https://www.typescriptlang.org/icons/icon-96x96.png?v=8944a05a8b601855de116c8a56d3b3ae'
		},
		{
			text: 'ts错误解读',
			link: 'https://www.tslang.cn/docs/handbook/error.html',
			icon: 'https://www.typescriptlang.org/icons/icon-96x96.png?v=8944a05a8b601855de116c8a56d3b3ae'
		},
		{
			text: 'ts演练场',
			link: 'https://www.typescriptlang.org/zh/play/',
			icon: 'https://www.typescriptlang.org/icons/icon-96x96.png?v=8944a05a8b601855de116c8a56d3b3ae'
		},
		{
			text: 'ts最新知识学习',
			link: 'https://github.com/any86/ts-log-cn',
			icon: 'https://www.typescriptlang.org/icons/icon-96x96.png?v=8944a05a8b601855de116c8a56d3b3ae'
		},
		{
			text: 'wangEditor',
			link: 'https://www.wangeditor.com/v5/',
			icon: 'https://www.wangeditor.com/favicon.ico',
			desc: '好用的富文本插件'
		},
		{
			text: 'quilljs',
			link: 'https://quilljs.com/docs/quickstart/',
			icon: 'https://quilljs.com/assets/images/favicon.ico'
		},
		{
			text: 'Mock.js',
			link: 'http://mockjs.com/',
			icon: 'http://mockjs.com/assets/img/logo-2.svg',
			desc: '数据模拟'
		},
		{
			text: 'Mock使用方法',
			link: 'https://www.jianshu.com/p/f3adb1aab09e',
			icon: 'http://mockjs.com/assets/img/logo-2.svg'
		},
		{
			text: 'Mock语法规范',
			link: 'https://www.jianshu.com/p/4579f40e6108',
			icon: 'http://mockjs.com/assets/img/logo-2.svg'
		},
		{
			text: '随机图片生成',
			link: 'https://source.unsplash.com/'
		},
		{
			text: 'JSON生成器',
			link: 'https://www.json-generator.com/'
		},
		{
			text: 'JQ22',
			link: 'https://www.jq22.com/'
		},
		{
			text: 'dowebok',
			link: 'http://www.dowebok.com/'
		},
		{
			text: '17素材',
			link: 'https://www.17sucai.com/',
			icon: 'https://www.17sucai.com/statics/images/favicon.ico'
		},
		{
			text: 'SVG代码优化',
			link: 'https://jakearchibald.github.io/svgomg/?utm_source=next.36kr.com'
		},
		{
			text: 'SVG图标',
			link: 'https://feathericons.com/',
			icon: 'https://feathericons.com/favicon-32x32.png'
		},
		{
			text: 'SVG压缩',
			link: 'https://www.zhangxinxu.com/sp/svgo/'
		},
		{
			text: '快速生成正则',
			link: 'https://github.com/Silence520/regexp'
		},
		{
			text: '🦕 any-rule',
			link: 'https://any86.github.io/any-rule/',
			icon: 'https://any86.github.io/any-rule/favicon.ico',
			desc: '很多实用的正则'
		},
		{
			text: '犸良',
			link: 'https://design.alipay.com/emotion',
			icon: 'https://gw.alipayobjects.com/mdn/rms_966c97/afts/img/A*5N6wSI7g83IAAAAAAAAAAAAAARQnAQ'
		},
		{
			text: 'Lottie素材',
			link: 'https://lottiefiles.com/',
			icon: 'https://lottiefiles.com/favicons-new/apple-icon-57x57.png'
		},
		{
			text: 'Lottie编辑器',
			link: 'https://editor.lottiefiles.com/',
			icon: 'https://lottiefiles.com/favicons-new/apple-icon-57x57.png'
		},
		{
			text: '一行js代码',
			link: 'https://1loc.dev/',
			icon: 'https://1loc.dev/favicon.svg'
		},
		{
			text: 'icon动画',
			link: 'http://bitshadow.github.io/iconate/',
			icon: 'https://s.cn.bing.net/th?id=ODLS.5a431bb9-ae72-4284-bad7-7360f84fad0d&w=40&h=40&o=6&pid=1.2'
		}
	]
} as BM.JsonList
