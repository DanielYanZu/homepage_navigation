export default {
	title: 'Vue',
	sort: 3,
	nav: [
		{
			text: 'Vue.js',
			link: 'https://staging-cn.vuejs.org/',
			icon: 'https://staging-cn.vuejs.org/logo.svg',
			desc: 'Vue官方文档'
		},
		{
			text: 'Pinia',
			link: 'https://pinia.esm.dev/',
			icon: 'https://pinia.esm.dev/logo.svg',
			desc: '状态管理器'
		},
		{
			text: 'Vue-router',
			link: 'https://router.vuejs.org/zh/',
			icon: 'https://staging-cn.vuejs.org/logo.svg',
			desc: 'vue路由官方文档'
		},
		{
			text: 'Vue-request',
			link: 'https://next.attojs.com/',
			icon: 'https://www.attojs.com/logo.png',
			desc: 'vue请求库'
		},
		{
			text: 'VueUse',
			link: 'https://vueuse.org/',
			icon: 'https://vueuse.org/favicon-32x32.png',
			desc: 'vue-hooks'
		},
		{
			text: 'mitt',
			link: 'https://www.npmjs.com/package/mitt',
			icon: 'https://camo.githubusercontent.com/9ad02d3d760df07fe6ed0428acd937ad8ffb871e4d825e4c435c4f195e872755/68747470733a2f2f692e696d6775722e636f6d2f42717358394e542e706e67',
			desc: '组件通信库'
		},
		{
			text: 'TDesign',
			link: 'https://tdesign.tencent.com/',
			icon: 'https://static.tdesign.tencent.com/favicon.ico',
			desc: '腾讯UI'
		},
		{
			text: 'Vue-demi',
			link: 'https://github.com/vueuse/vue-demi',
			desc: '同时生成vue3和vue2代码的框架'
		},
		{
			text: 'vuepress',
			link: 'https://v2.vuepress.vuejs.org/zh/',
			icon: 'https://v2.vuepress.vuejs.org/images/hero.png',
			desc: 'vue文档框架'
		},
		{
			text: 'element-plus',
			link: 'https://element-plus.org/zh-CN/',
			icon: 'https://element-plus.org/images/element-plus-logo-small.svg',
			desc: '饿了么vue UI'
		},
		{
			text: 'vant',
			link: 'https://vant-ui.github.io/vant/#/zh-CN#jie-shao',
			icon: 'https://cdn.jsdelivr.net/npm/@vant/assets/logo.png',
			desc: '有赞vue-ui'
		},
		{
			text: 'vant-theme',
			link: 'https://aisen60.gitee.io/vant-theme/src/page/theme/index.html',
			icon: 'https://cdn.jsdelivr.net/npm/@vant/assets/logo.png',
			desc: 'vant主题设置'
		},
		{
			text: 'kingCloudDesign',
			link: 'https://design.ksyun.com/',
			icon: 'https://damife.ks3-cn-beijing.ksyuncs.com/kpc/fonts/header_logo.fbb0da19b5ac7c683e512bfb4588c6a5.png',
			desc: '金山出品的一个vue3-UI'
		},
		{
			text: 'NutUI',
			link: 'https://nutui.jd.com/#/',
			icon: 'https://img14.360buyimg.com/imagetools/jfs/t1/167902/2/8762/791358/603742d7E9b4275e3/e09d8f9a8bf4c0ef.png',
			desc: '京东vue3移动端UI'
		},
		{
			text: 'arco-design',
			link: 'https://arco.design/',
			icon: 'https://unpkg.byted-static.com/latest/byted/arco-config/assets/favicon.ico',
			desc: '字节跳动的Vue-UI'
		},
		{
			text: 'Naive UI',
			link: 'https://www.naiveui.com/zh-CN/light',
			icon: 'https://www.naiveui.com/assets/naivelogo.93278402.svg',
			desc: '尤雨溪推荐的Vue3-ui'
		},
		{
			text: 'Plain UI',
			link: 'http://plain-pot.gitee.io/plain-ui-doc/#home%2Fintroduce.entire',
			icon: 'http://plain-pot.gitee.io/plain-ui-doc/favicon.ico'
		},
		{
			text: 'ant-D',
			link: 'https://next.antdv.com/components/overview/',
			icon: '//aliyuncdn.antdv.com/favicon.ico',
			desc: '蚂蚁的Vue3-UI'
		},
		{
			text: 'virtual-scroller',
			link: 'https://akryum.github.io/vue-virtual-scroller/#/',
			icon: 'https://staging-cn.vuejs.org/logo.svg',
			desc: '无限滚动，性能最好的插件'
		},
		{
			text: 'vcalendar',
			link: 'https://vcalendar.netlify.com/',
			icon: 'https://vcalendar.netlify.app/hero.png',
			desc: '日历插件'
		},
		{
			text: 'vue-draggable',
			link: 'https://sortablejs.github.io/vue.draggable.next/#/handle',
			desc: '拖拽插件'
		},
		{
			text: 'vue-clipboard',
			link: 'https://vue-clipboard2.inndy.tw/',
			desc: '剪切板插件'
		},
		{
			text: 'vxetable',
			link: 'https://vxetable.cn/#/table/start/install',
			icon: 'https://vxetable.cn/logo.png',
			desc: '一款好用的vue表格'
		},
		{
			text: 'Element-UI',
			link: 'https://element.eleme.cn/2.13/#/zh-CN/',
			icon: 'https://element.eleme.cn/favicon.ico'
		},
		{
			text: 'Revue-drag',
			link: 'https://revue-draggable.vercel.app/',
			desc: '基于vue的拖拽工具'
		},
		{
			text: 'TroisJS',
			link: 'https://troisjs.github.io/',
			desc: 'vue3+three.js'
		},
		{
			text: 'vue3工具集',
			link: 'https://hu-snail.github.io/vue3-resource/',
			desc: '其他的工具集'
		}
	]
} as BM.JsonList
