export default {
	title: 'CSS',
	sort: 4,
	nav: [
		{
			text: 'Tailwindcss',
			link: 'https://tailwindcss.com/',
			icon: 'https://www.tailwindcss.cn/favicon-32x32.png',
			desc: '超好用的css框架'
		},
		{
			text: 'daisyUI',
			link: 'https://daisyui.com/?from=thosefree.com&lang=zh_cn',
			desc: 'tailwindcssUI组件',
			icon: 'https://daisyui.com/images/default.jpg'
		},
		{
			text: 'prelineUI',
			link: 'https://preline.co/',
			icon: 'https://preline.co/preline-logo.svg',
			desc: 'tailwind全响应UI'
		},
		{
			text: 'CSS集合',
			link: 'https://qishaoxuan.github.io/css_tricks/createTriangle/'
		},
		{
			text: 'magic-CSS',
			link: 'https://www.minimamente.com/project/magic/',
			icon: 'https://www.minimamente.com/project/magic/assets/images/magic_big.png'
		},
		{
			text: 'bttn.css',
			link: 'https://bttn.surge.sh/'
		},
		{
			text: 'Animate.css',
			link: 'https://animate.style/',
			icon: 'https://animate.style/img/animatecss-opengraph.jpg'
		},
		{
			text: 'grid-layout',
			link: 'https://grid.layoutit.com/',
			icon: 'https://grid.layoutit.com/favicon.ico'
		},
		{
			text: 'flex-box学习',
			link: 'https://darekkay.com/flexbox-cheatsheet/',
			icon: 'https://darekkay.com/favicon.ico'
		},
		{
			text: '背景平铺',
			link: 'https://leaverou.github.io/css3patterns/#'
		},
		{
			text: '形状生成',
			link: 'http://tools.jb51.net/code/css3path'
		},
		{
			text: '投影生成',
			link: 'https://www.html.cn/tool/css3Preview/Box-Shadow.html'
		},
		{
			text: '动画工具',
			link: 'https://www.w3cways.com/css3-animation-tool'
		},
		{
			text: '文本绕排',
			link: 'http://csswarp.eleqtriq.com/'
		},
		{
			text: '网格生成工具',
			link: 'https://cssgrid-generator.netlify.app/',
			icon: 'https://cssgrid-generator.netlify.app/favicon.ico'
		},
		{
			text: 'grabient',
			link: 'https://www.grabient.com/',
			icon: 'https://www.grabient.com/favicon-32x32.png',
			desc: '渐变生成'
		}
	]
}
