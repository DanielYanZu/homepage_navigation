export default {
	title: '微信',
	sort: 4,
	nav: [
		{
			text: '微信公众平台',
			link: 'https://mp.weixin.qq.com/',
			icon: 'https://res.wx.qq.com/a/wx_fed/assets/res/OTE0YTAw.png'
		},
		{
			text: '微信小程序',
			link: 'https://developers.weixin.qq.com/miniprogram/dev/framework/',
			icon: 'https://res.wx.qq.com/a/wx_fed/assets/res/OTE0YTAw.png'
		},
		{
			text: 'Taro',
			link: 'https://taro.jd.com/',
			icon: 'https://taro-docs.jd.com/taro/img/taroLogo180.png'
		},
		{
			text: 'Uni-app',
			link: 'https://uniapp.dcloud.io/',
			icon: 'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-a90b5f95-90ba-4d30-a6a7-cd4d057327db/d23e842c-58fc-4574-998d-17fdc7811cc3.png?v=1556263038788'
		},
		{
			text: 'Vant Weapp',
			link: 'https://youzan.github.io/vant-weapp/#/intro',
			icon: 'https://img.yzcdn.cn/public_files/2017/12/18/fd78cf6bb5d12e2a119d0576bedfd230.png',
			desc: 'vant的小程序版本'
		},
		{
			text: '小程序转icon',
			link: 'https://transfonter.org/'
		}
	]
} as BM.JsonList
