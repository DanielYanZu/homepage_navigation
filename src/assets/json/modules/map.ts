export default {
	title: '地图&图表',
	sort: 7,
	nav: [
		{
			text: '百度JSAPI',
			link: 'https://lbsyun.baidu.com/index.php?title=jspopular3.0',
			icon: 'https://lbsyun.baidu.com/skins/MySkin/resources/img/icon/lbsyunlogo_icon.ico'
		},
		{
			text: '百度GL',
			link: 'https://lbsyun.baidu.com/index.php?title=jspopularGL',
			icon: 'https://lbsyun.baidu.com/skins/MySkin/resources/img/icon/lbsyunlogo_icon.ico'
		},
		{
			text: '百度mapV',
			link: 'https://lbsyun.baidu.com/solutions/mapvdata',
			icon: 'https://lbsyun.baidu.com/skins/MySkin/resources/img/icon/lbsyunlogo_icon.ico'
		},
		{
			text: '拾取坐标',
			link: 'http://api.map.baidu.com/lbsapi/getpoint/index.html',
			icon: 'https://lbsyun.baidu.com/skins/MySkin/resources/img/icon/lbsyunlogo_icon.ico'
		},
		{
			text: '百度地图生成器',
			link: 'http://api.map.baidu.com/lbsapi/createmap/index.html',
			icon: 'https://lbsyun.baidu.com/skins/MySkin/resources/img/icon/lbsyunlogo_icon.ico'
		},
		{
			text: '定位组件',
			link: 'https://lbs.qq.com/tool/component-geolocation.html',
			icon: 'https://mapapi.qq.com/web/lbs/logo/favicon.ico'
		},
		{
			text: '腾讯API',
			link: 'https://lbs.qq.com/webApi/javascriptV2/jsGuide/jsOverview',
			icon: 'https://mapapi.qq.com/web/lbs/logo/favicon.ico'
		},
		{
			text: '坐标拾取器',
			link: 'https://lbs.qq.com/tool/getpoint/index.html',
			icon: 'https://mapapi.qq.com/web/lbs/logo/favicon.ico'
		},
		{
			text: '高德API',
			link: 'https://lbs.amap.com/api/jsapi-v2/summary/',
			icon: '//a.amap.com/pc/static/favicon.ico'
		},
		{
			text: '百度腾讯转坐标',
			link: 'https://www.jianshu.com/p/687386d13a08'
		},
		{
			text: 'echarts',
			link: 'https://echarts.apache.org/zh/index.html',
			icon: 'https://fastly.jsdelivr.net/gh/apache/echarts-website@asf-site/zh/images/favicon.png?_v_=20200710_1',
			desc: '图表库'
		},
		{
			text: 'ant-v',
			link: 'https://antv.vision/',
			icon: 'https://antv.vision/icons/icon-48x48.png?v=9772447a8d07a8fe19894b5176c6cb0d'
		},
		{
			text: 'highcharts',
			link: 'http://www.hcharts.cn/',
			icon: 'https://s1.jscdn.com.cn/highcharts/images/favicon.ico'
		},
		{
			text: 'echarts图例',
			link: 'http://www.ppchart.com/#/',
			icon: 'http://www.ppchart.com/favicon.ico'
		},
		{
			text: 'echarts社区',
			link: 'https://www.makeapie.cn/echarts',
			icon: 'https://fastly.jsdelivr.net/gh/apache/echarts-website@asf-site/zh/images/favicon.png?_v_=20200710_1'
		}
	]
} as BM.JsonList
