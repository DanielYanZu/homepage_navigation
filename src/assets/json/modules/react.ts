export default {
	title: 'React',
	sort: 2,
	nav: [
		{
			text: 'React',
			link: 'https://zh-hans.reactjs.org/',
			icon: 'https://react.dev/images/og-home.png',
			desc: 'React官方文档'
		},
		{
			text: 'React-router',
			link: 'https://reactrouter.com/en/main',
			icon: 'https://reactrouter.com/favicon-light.png',
			desc: 'React路由'
		},
		{
			text: 'Ant-design',
			link: 'https://ant.design/index-cn',
			icon: 'https://gw.alipayobjects.com/zos/antfincdn/UmVnt3t4T0/antd.png',
			desc: '蚂蚁UI'
		},
		{
			text: 'Ant-d-mobile',
			link: 'https://mobile.ant.design/zh',
			icon: 'https://gw.alipayobjects.com/zos/bmw-prod/69a27fcc-ce52-4f27-83f1-c44541e9b65d.svg',
			desc: 'AntD移动端UI'
		},
		{
			text: 'a-hooks',
			link: 'https://ahooks.js.org/zh-CN',
			icon: 'https://ahooks.js.org/zh-CN/logo.svg',
			desc: '实用的reactHooks'
		},
		{
			text: 'redux',
			link: 'https://www.reduxjs.cn/',
			icon: 'https://www.reduxjs.cn/img/redux-logo-landscape.png',
			desc: 'react状态管理器'
		},
		{
			text: 'arco-design',
			link: 'https://arco.design/react/docs/start',
			icon: 'https://unpkg.byted-static.com/latest/byted/arco-config/assets/favicon.ico',
			desc: '字节React-UI'
		},
		{
			text: 'arco-mobile',
			link: 'https://arco.design/mobile/react',
			icon: 'https://unpkg.byted-static.com/latest/byted/arco-config/assets/favicon.ico',
			desc: '字节移动端-UI'
		},
		{
			text: 'Umi',
			link: 'https://umijs.org/',
			icon: 'https://img.alicdn.com/tfs/TB1YHEpwUT1gK0jSZFhXXaAtVXa-28-27.svg',
			desc: 'react-企业级框架'
		},
		{
			text: 'NutUI-react',
			link: 'https://nutui.jd.com/next/#/',
			icon: 'https://img14.360buyimg.com/imagetools/jfs/t1/167902/2/8762/791358/603742d7E9b4275e3/e09d8f9a8bf4c0ef.png',
			desc: '京东的移动端UI'
		},
		{
			text: 'react-spring',
			link: 'https://www.react-spring.dev/',
			icon: 'https://camo.githubusercontent.com/973c99d17e4ce72d08c4433449045d8391948711f11ac5f328a585e2a7bc8663/68747470733a2f2f692e696d6775722e636f6d2f515a6f776e68672e706e67',
			desc: '动画库'
		},
		{
			text: 'react-use',
			link: 'https://streamich.github.io/react-use/?path=/story/components-usekey--demo',
			desc: '好用的Hooks库'
		},
		{
			text: 'semiDesign',
			link: 'https://semi.design/zh-CN/',
			desc: '抖音前端UI',
			icon: 'https://lf9-static.semi.design/obj/semi-tos/images/favicon.ico'
		},
		{
			text: 'proComp',
			link: 'https://procomponents.ant.design/',
			desc: 'ant-d高阶组件',
			icon: 'https://gw.alipayobjects.com/zos/rmsportal/rlpTLlbMzTNYuZGGCVYM.png'
		},
		{
			text: 'Recoil',
			link: 'https://recoiljs.org/zh-hans/',
			icon: 'https://recoiljs.org/zh-hans/img/logo.svg',
			desc: '状态管理器'
		}
	]
} as BM.JsonList
