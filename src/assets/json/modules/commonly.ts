export default {
	title: '推荐',
	sort: 1,
	nav: [
		{
			text: 'svelte',
			link: 'https://svelte.dev/',
			icon: 'https://svelte.dev/favicon.png',
			desc: 'svelte框架'
		},
		{
			text: '掘金',
			link: 'https://juejin.im/timeline',
			icon: 'https://lf3-cdn-tos.bytescm.com/obj/static/xitu_juejin_web//static/favicons/apple-touch-icon.png',
			desc: '前端技术论坛'
		},
		{
			text: '码云',
			link: 'https://gitee.com/',
			icon: 'https://gitee.com/static/images/logo_themecolor.png',
			desc: '国内代码仓库'
		},
		{
			text: '知乎',
			link: 'https://www.zhihu.com/signin?next=%2F',
			icon: 'https://static.zhihu.com/heifetz/assets/apple-touch-icon-152.a53ae37b.png',
			desc: '有问题，上知乎'
		},
		{
			text: '蓝湖',
			link: 'https://lanhuapp.com',
			icon: 'https://lhcdn.lanhuapp.com/web/imgs/lanhuLogo1db1cd87.svg',
			desc: '设计协同平台'
		},
		{
			text: '腾讯翻译君',
			link: 'https://fanyi.qq.com/',
			icon: 'https://stdl.qq.com/stdl/activity/translate_pcweb/imgs/header_logo_31O-BS.png'
		},
		{
			text: '阿里图标库',
			link: 'http://www.iconfont.cn/',
			icon: '//img.alicdn.com/imgextra/i4/O1CN01EYTRnJ297D6vehehJ_!!6000000008020-55-tps-64-64.svg'
		},
		{
			text: '字节图标库',
			link: 'https://iconpark.oceanengine.com/home',
			icon: 'https://lf1-cdn2-tos.bytegoofy.com/bydesign/iconparksite/static/media/logo_with_name.598fc011.svg'
		}
	]
} as BM.JsonList
