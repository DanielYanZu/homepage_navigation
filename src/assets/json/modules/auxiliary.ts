export default {
	title: '辅助工具',
	sort: 9,
	nav: [
		{
			text: 'collectUI',
			link: 'https://collectui.com/designs'
		},
		{
			text: '渐变配色',
			link: 'https://uigradients.com/#EmeraldWater',
			icon: 'https://uigradients.com/static/images/favicon-32x32.png'
		},
		{
			text: '配色',
			link: 'https://colorhunt.co/?tdsourcetag=s_pctim_aiomsg',
			icon: 'https://colorhunt.co/img/colorhunt-favicon.svg?2'
		},
		{
			text: 'Adobe Color CC',
			link: 'https://color.adobe.com/zh/create/color-wheel/',
			icon: 'https://color.adobe.com/apple-touch-icon.png'
		},
		{
			text: '矢量插画素材',
			link: 'https://undraw.co/illustrations',
			icon: 'https://undraw.co/favicon.ico'
		},
		{
			text: '交互赏析',
			link: 'https://uimovement.com/?tdsourcetag=s_pctim_aiomsg',
			icon: 'https://screenlane.com/static/website/images/icons/apple-icon-60x60.png'
		}
	]
} as BM.JsonList
