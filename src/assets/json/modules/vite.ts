export default {
	title: 'vite',
	sort: 6,
	nav: [
		{
			text: 'Vite',
			link: 'https://cn.vitejs.dev/',
			icon: 'https://cn.vitejs.dev/logo.svg',
			desc: '下一代前端开发与构建工具'
		},
		{
			text: 'htmlEnv',
			link: 'https://www.npmjs.com/package/vite-plugin-html-env',
			desc: '给入口文件添加变量'
		}
	]
} as BM.JsonList
