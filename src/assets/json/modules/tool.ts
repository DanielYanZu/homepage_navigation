export default {
	title: 'JS工具',
	sort: 8,
	nav: [
		{
			text: 'Axios',
			link: 'https://www.axios-http.cn/',
			icon: 'https://www.axios-http.cn/assets/favicon.ico',
			desc: '请求工具'
		},
		{
			text: 'big.js',
			link: 'https://mikemcl.github.io/big.js/',
			desc: '前端数学计算工具'
		},
		{
			text: 'day.js',
			link: 'https://dayjs.fenxianglu.cn/',
			icon: 'https://dayjs.fenxianglu.cn/assets/favicon.png',
			desc: '前端日期处理工具'
		},
		{
			text: 'math.js',
			link: 'https://mathjs.org/index.html',
			icon: 'https://mathjs.org/css/img/mathjs_330x100.png',
			desc: '数学库'
		},
		{
			text: 'lodash',
			link: 'https://www.html.cn/doc/lodash/',
			icon: 'https://www.html.cn/doc/lodash/assets/img/lodash.svg',
			desc: '一款实用的工具类'
		},
		{
			text: 'numeral.js',
			link: 'http://numeraljs.com/',
			desc: '数字格式化'
		},
		{
			text: 'hammer.js',
			link: 'http://hammerjs.github.io/getting-started/',
			icon: 'http://hammerjs.github.io/assets/img/favicon.ico',
			desc: '移动端手势处理库'
		},
		{
			text: 'cookie.js',
			link: 'https://github.com/jaywcjlove/cookie.js',
			desc: 'cookie插件'
		},
		{
			text: 'dexie.js',
			link: 'https://dexie.org/',
			icon: 'https://dexie.org/assets/images/favicon-yuri.png',
			desc: 'indexDB工具类'
		},
		{
			text: 'is.js',
			link: 'http://is.js.org/',
			icon: 'http://is.js.org/assets/img/is-js-logo.png',
			desc: '好用的判断类'
		}
	]
} as BM.JsonList
